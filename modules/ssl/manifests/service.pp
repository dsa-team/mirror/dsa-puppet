# deploy an SSL certificate
#
# @param ensure
#   set to "absent" to destroy this resource,
#   set to "ifstatic" to include the cert if we have the static
#   component of the certname.
#
# @param certname
#   the FQDN of the certificate to deploy, defaults to $name
#
# @param tlsaport
#   an array of ports for which to create TLSA records in DNS using
#   dnsextras::tlsa_record for the given certname. defaults to 443, set
#   to [] to disable.
#
# @param notify
#   restart the given resources on file changes
define ssl::service (
  Enum['present','absent','ifstatic'] $ensure = 'present',
  Variant[Stdlib::Port, Array[Stdlib::Port]] $tlsaport = 443,
  Any $notify = [],
  Stdlib::Host $certname = $name,
) {
  if ($ensure == 'ifstatic') {
    $ssl_ensure = has_static_component($certname) ? {
      true => 'present',
      false => 'absent'
    }
  } else {
    $ssl_ensure = $ensure
  }

  file { "/etc/ssl/debian/certs/${certname}.crt":
    ensure  => $ssl_ensure,
    content => template('ssl/crt.erb'),
    notify  => [ Exec['refresh_debian_hashes'], $notify ],
  }
  file { "/etc/ssl/debian/certs/${certname}.crt-chain":
    ensure  => $ssl_ensure,
    content => template('ssl/crt-chain.erb'),
    notify  => [ $notify ],
    links   => follow,
  }
  file { "/etc/ssl/debian/certs/${certname}.crt-chained":
    ensure  => $ssl_ensure,
    content => template('ssl/crt-chained.erb'),
    notify  => [ $notify ],
  }

  file { "/etc/ssl/private/${certname}.key":
    ensure  => $ssl_ensure,
    mode    => '0440',
    group   => 'ssl-cert',
    content => template('ssl/key.erb'),
    notify  => [ $notify ],
    links   => follow,
  }
  file { "/etc/ssl/private/${certname}.key-certchain":
    ensure  => $ssl_ensure,
    mode    => '0440',
    group   => 'ssl-cert',
    content => template('ssl/key-chained.erb'),
    notify  => [ $notify ],
    links   => follow,
  }

  $tlsaports = Array($tlsaport, true)
  if (size($tlsaports) > 0 and $ssl_ensure == 'present') {
    $portlist = join($tlsaports, '-')
    $certdir = hiera('paths.letsencrypt_dir')
    dnsextras::tlsa_record{ "tlsa-${certname}-${portlist}":
      certfile => [ "${certdir}/${certname}.crt" ],
      port     => $tlsaport,
      hostname => $certname,
    }
  }
}
