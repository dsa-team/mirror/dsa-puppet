Facter.add("systemproductname") do
	confine :kernel => :linux
	setcode do
		if FileTest.exist?("/usr/sbin/dmidecode")
			%x{/usr/sbin/dmidecode -s system-product-name}.chomp.strip
		else
			false
		end
	end
end

Facter.add("hw_can_temp_sensors") do
	confine :kernel => :linux
	setcode do
		if FileTest.exist?("/sys/devices/virtual/thermal/thermal_zone0/temp") or Dir.glob("/sys/class/hwmon/hwmon*/temp*input").any?
			true
		else
			false
		end
	end
end

Facter.add("hw_can_fan_sensors") do
	confine :kernel => :linux
	setcode do
		if Dir.glob("/sys/class/hwmon/hwmon*/fan*input").any?
			true
		else
			false
		end
	end
end

Facter.add("hw_can_hwrng") do
	confine :kernel => :linux
	setcode do
		if FileTest.exist?("/dev/hwrng") and FileTest.exist?("/sys/class/misc/hw_random/rng_current")
			x = File.open("/sys/class/misc/hw_random/rng_current").read
			if x =~ /none/
				false
			else
				true
			end
		else
			false
		end
	end
end
