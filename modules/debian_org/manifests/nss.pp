# allow to switch NSS providers
class debian_org::nss (
  Enum['db', 'cache'] $provider = 'db',
) {
  $package = $provider ? {
    cache   => 'libnss-cache',
    default => 'libnss-db',
  }

  package { $package:
    ensure => installed,
  }

  file { '/etc/nsswitch.conf':
    mode    => '0444',
    content => template('debian_org/nsswitch.conf.erb'),
  }
}
