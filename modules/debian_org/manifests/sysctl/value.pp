# Create a sysctl entry
#
# @param key    The key to set
# @param value  The value to set it to
define debian_org::sysctl::value (
  Variant[Integer, String] $value,
  String $key = $name,
) {
  include debian_org::sysctl
  concat_fragment { "sysctl-${name}":
    target  => assert_type(Stdlib::Absolutepath, $debian_org::sysctl::puppet_sysctl_file),
    content => "${key} = ${value}\n",
  }
}
