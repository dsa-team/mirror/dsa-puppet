# collect authorized_keys stored using authorized_key_add

define ssh::authorized_key_collect(
  String $target_user,
  String $collect_tag,
) {
  concat { "/etc/ssh/puppetkeys/${target_user}":
    warn           => '# This file is maintained with puppet',
    ensure_newline => true,
    mode           => '0444',
  }
  Concat::Fragment <<| tag == "ssh::authorized_key::fragment::${collect_tag}::${target_user}" |>>

  Ferm::Rule <<| tag == "ssh::authorized_key::ferm::${collect_tag}::${target_user}" |>>
}
