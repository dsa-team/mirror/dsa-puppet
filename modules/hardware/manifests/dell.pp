# Set up hardware support and monitoring for Dell servers
class hardware::dell {
  # To get the OpenManage packages
  include debian_org::apt_restricted

  package { 'srvadmin-server-cli':
    ensure => installed,
    tag    => extra_repo,
  }
  package { 'srvadmin-storage-cli':
    ensure => installed,
    tag    => extra_repo,
  }
  package { 'srvadmin-omcommon':
    ensure => installed,
    tag    => extra_repo,
  }
  # Various OpenManage libraries require libcrypto.so.1.0.0
  # but the Dell packages don't have a relevant dependency
  package { 'libssl1.0.0':
    ensure => installed,
    tag    => extra_repo,
  }
  package { 'libxslt1.1':
    ensure => installed,
  }

  # buggy and conflicts with Dell's own thing
  dsa_systemd::mask { 'openipmi.service': }

  mon::service::shell_wrapped { 'hardware-dell-openmanage': # {{{
    check_interval => '1h',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/dsa-check-openmanage',
    sudo           => true,
  } # }}}
}
