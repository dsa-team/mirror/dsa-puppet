class hardware::raid {
	include hardware::raid::proliant

	include hardware::raid::raidmpt
	include hardware::raid::megaraid_sas
}
