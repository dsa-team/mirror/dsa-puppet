# Set up necessary modules and stuff for Lenovo servers
class hardware::lenovo {
  # load modules for OOB virtual media and input devices
  base::linux_module { 'cdc_ether': }
  base::linux_module { 'hid_generic': }
  base::linux_module { 'joydev': }
  base::linux_module { 'sr_mod': }
  base::linux_module { 'uas': }
  base::linux_module { 'usbhid': }

  # TODO: health monitoring
}
