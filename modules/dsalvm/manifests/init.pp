# put a lvm.conf file on the host,
#   optionally with the global_filter attribute set,
#   with issue_discards set as configured.
class dsalvm(
  Optional[String] $global_filter = undef,
  Boolean          $issue_discards = false,
) {
  file { '/etc/lvm/lvm.conf':
    ensure  => file,
    content => template('dsalvm/lvm.conf-buster.erb'),
    notify  => [ Exec['update-initramfs -u'], Exec['vgscan'] ];
  }

  exec { 'vgscan':
      path        => '/etc/init.d:/usr/bin:/usr/sbin:/bin:/sbin',
      refreshonly => true;
  }
}
