# configure syslog-ng
class syslog_ng {
  package { 'syslog-ng':
    ensure => installed
  }

  $query = 'nodes[certname] { resources { type = "Class" and title = "Roles::Loghost" } }'
  $loghosts = sort(puppetdb_query($query).map |$value| { $value["certname"] })
  $v6only = ($base::public_address == undef)
  $altdestport = lookup( { 'name' => 'syslog::use_alt_port', 'default_value' => false } )
  $destport = $altdestport ? {
    true => 443,
    default => 5140,
  }

  service { 'syslog-ng':
    ensure    => running,
    hasstatus => false,
    pattern   => 'syslog-ng',
    subscribe => Exec['refresh_debian_hashes'],
  }

  file { '/etc/syslog-ng/syslog-ng.conf':
    content => template('syslog_ng/syslog-ng.conf.erb'),
    require => Package['syslog-ng'],
    notify  => Service['syslog-ng']
  }
  file { '/etc/default/syslog-ng':
    source  => 'puppet:///modules/syslog_ng/syslog-ng.default',
    require => Package['syslog-ng'],
    notify  => Service['syslog-ng']
  }
  file { '/etc/logrotate.d/syslog-ng':
    source  => 'puppet:///modules/syslog_ng/syslog-ng.logrotate',
    require => Package['syslog-ng']
  }

  file { '/etc/systemd/system/syslog-ng.service':
    source => 'puppet:///modules/syslog_ng/syslog-ng.service',
    notify => Exec['systemctl daemon-reload'],
  }

  file { '/etc/systemd/system/syslog.service':
    ensure => absent,
    notify => Exec['systemctl daemon-reload'],
  }

  $loghosts.each |$loghost| {
    @@mon::service::shell_wrapped { "remote-logging-${loghost}-${trusted['certname']}": # {{{
      tag          => "loghost-check-to-${loghost}",
      host_name    => $trusted['certname'],
      display_name => "remote-logging-${loghost}",
      command      => "/usr/lib/nagios/plugins/dsa-check-log-age-loghost ${trusted['hostname']}",
    } # }}}
  }
}
