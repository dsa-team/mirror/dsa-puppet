# define and enable (or disable) an .automount activated .mount
#
define dsa_systemd::automount(
  String $where,
  String $what,
  Optional[String] $fstype = undef,
  Optional[String] $options = undef,
  Enum['present','absent'] $ensure = 'present',
) {
  $mount_file = "/etc/systemd/system/${name}.mount"
  $automount_file = "/etc/systemd/system/${name}.automount"

  $action = $ensure ? {
    present => 'enable',
    default => 'disable',
  }

  file { $mount_file:
    ensure  => $ensure,
    content => template('dsa_systemd/automount-mount.erb'),
  }

  file { $automount_file:
    ensure  => $ensure,
    content => template('dsa_systemd/automount-automount.erb'),
    notify  => Exec["systemctl ${action} ${automount_file}"],
    require => [File[$mount_file]],
  }

  exec { "systemctl ${action} ${automount_file}":
    command     => "systemctl ${action} $(systemd-escape -p --suffix=automount ${where})",
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    refreshonly => true,
    provider    => shell,
    notify      => Exec['systemctl daemon-reload'],
  }
}
