class stunnel4 {

	package { 'stunnel4':
		ensure => installed
	}

	file { '/etc/stunnel':
		ensure  => directory,
		mode    => '0755',
	}
	file { '/etc/stunnel/stunnel.conf':
		ensure  => absent,
		require => Package['stunnel4'],
	}

	exec { 'disable_stunnel4':
		command => 'sed -i -e \'/^ENABLED=1.*/d\' /etc/default/stunnel4',
		unless  => 'grep -q -v \'^ENABLED=1\' /etc/default/stunnel4',
		require => Package['stunnel4'],
	}
	service { 'stunnel4':
		ensure  => stopped,
		enable  => false,
		require => Package['stunnel4'],
	}
	file { '/etc/tmpfiles.d/stunnel.conf':
		content => 'd /var/run/stunnel4 0755 stunnel4 stunnel4 -',
		notify  => Exec['systemd-tmpfiles --create --exclude-prefix=/dev'],
		require => Package['stunnel4'],
	}

	if (versioncmp($::lsbmajdistrelease, '11') >= 0) {
		file { '/etc/systemd/system/stunnel@.service':
			ensure => absent,
		}
	} else {
		file { '/etc/systemd/system/stunnel@.service':
			content => @(EOF)
				[Unit]
				Description=TLS tunnel for network daemons - per-config-file service
				Documentation=man:stunnel4(8)
				PartOf=stunnel.target

				[Service]
				ExecStart=/usr/bin/stunnel4 /etc/stunnel/%i.conf
				PrivateTmp=yes

				[Install]
				WantedBy=multi-user.target
				| EOF
		}
	}
}
