# = Class: multipath
#
# Manage a multipath installation
#
# == Sample Usage:
#
#   include multipath
#
class multipath {
  case $::hostname {
    default: {
      $conffile = ''
    }
  }

  if $conffile != '' {
    package { 'multipath-tools':
      ensure => installed,
    }
    exec { 'multipath reload':
      path        => '/usr/bin:/usr/sbin:/bin:/sbin',
      command     => 'service multipath-tools reload',
      refreshonly => true,
      require     =>  Package['multipath-tools'],
    }

    file { '/etc/multipath.conf':
      content => template("multipath/${conffile}.erb"),
      notify  => Exec['multipath reload']
    }
  }
}
