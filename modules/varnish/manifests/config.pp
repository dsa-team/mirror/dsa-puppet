define varnish::config (
	$listen = [':6081'],
	$source=undef,
	$content=undef,
	$ensure = 'present',
	Array[String] $storage = ['malloc,256m'],
) {
	if $name != 'default' {
		fail ( 'This module cannot setup non-default varnish instances yet.' )
	}

	case $ensure {
		present: {
			include varnish::base

			if ! ($source or $content) {
				fail ( "No configuration found for ${name}" )
			}

			$listenarr = [] + $listen
			$listenstr = join(prefix($listenarr, '-a '), ' ')
			dsa_systemd::override { 'varnish':
				content => template('varnish/varnishd.service.erb'),
			}

			$dest = "/etc/varnish/${name}.vcl"
			if $content {
				file { $dest:
					ensure  => $ensure,
					content => $content,
					notify  => Service['varnish'],
				}
			} elsif $source {
				file { $dest:
					ensure => $ensure,
					source => $source,
					notify => Service['varnish'],
					}
			}
		}
		default: {
			fail ( 'Can only deal with ensure=>present for now' )
		}
	}
}
