# base class for Debian's mirror-health checker
class mirror_health () {
  ensure_packages(['python3-requests'], { ensure => 'installed' })

  $script = '/usr/local/sbin/mirror-health'
  $confdir = '/etc/dsa/health-check'

  file { $script:
    source => 'puppet:///modules/mirror_health/mirror-health',
    mode   => '0555',
  }

  file { $confdir:
    ensure  => 'directory',
    purge   => true,
    force   => true,
    recurse => true,
    mode    => '0755';
  }
}
