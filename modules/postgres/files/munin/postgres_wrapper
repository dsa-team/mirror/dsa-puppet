#!/bin/bash

# Wrapper for munin PostgreSQL plugins to add cluster support to plugins
# without native support.
#
# If called as postgres_thing_VERSION_CLUSTERNAME, attempts to locate a
# cluster with the correct name and version, and runs postgres_thing
# with PGPORT set appropriately.
#
# Copyright 2020 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# Originally based on code which was:
# Copyright 2007, 2011 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#%# family=auto
#%# capabilities=autoconf suggest

findconfig() {
    local conf
    local thisver thiscluster thisport
    conf=$(echo $0 | sed -e "s/^.*_\(.*\)_\(.*\)$/\1_\2/")
    if [ -z "$conf" ]; then
        echo >&2 "Do not know for which cluster to get stats"
        exit 1
    fi

    mycluster=$(
        pg_lsclusters --no-header | \
        while read thisver thiscluster thisport dummy; do
            if [ "${thisver}_${thiscluster}" = "$conf" ] ; then
                echo $thisport $thisver $thiscluster
                break
            fi
        done
    )

    if [ -z "$mycluster" ]; then
        echo >&2 "Did not find port for $conf"
        exit 1
    fi

    read port ver cluster <<< "$mycluster"
}

td=$(mktemp -d) || exit
trap "rm -rf -- '$td'" EXIT

plugindir=${plugindir:-${MUNIN_LIBDIR}/plugins}

# attempt to deduce the actual plugin which should be called
# - remove the cluster information
plugin="$(basename "$0" | sed -e "s/^\(.*\)_.*_.*$/\1/")"
if [ -e "$plugindir/$plugin" ]; then
    # - if our first guess exists, use that
    realplugin=$plugin
else
    # - otherwise, assume this is a wildcard plugin, and
    #   strip the final part
    realplugin="$(echo $plugin | sed -e "s/^\(.*_\).*$/\1/")"
fi

# create a temporary plugin which we can call using the
# name that it would expect
tempplugin="$td/$plugin"
ln -s "$plugindir/$realplugin" "$tempplugin"

if [ "$1" = "autoconf" ]; then
    if which pg_lsclusters > /dev/null 2>&1 ; then
        echo yes
    else
        echo "no (pg_lsclusters not available)"
    fi
elif [ "$1" = "suggest" ]; then
    pg_lsclusters --no-header | while read ver cluster dummy; do
        echo "${ver}_${cluster}"
    done
else
    findconfig
    PGPORT="$port" "$tempplugin" "$@" |
        sed -e "/^graph_title/ s/$/\0 for $cluster (pg $ver on port $port)/"
fi
