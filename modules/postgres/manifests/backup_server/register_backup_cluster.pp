#
# Cause our pg backup servers to run base backups of
# this postgres cluster and add the cluster to the
# monitoring configuration.
#
# @param hostname     name of the db server
# @param fqdn         fqdn of the db server
# @param pg_port      port of the postgres cluster
# @param pg_role      replication role username
# @param pg_password  password of the replication role
# @param pg_cluster   cluster name
# @param pg_version   pg version of the cluster
define postgres::backup_server::register_backup_cluster (
  Integer $pg_port,
  String $pg_role,
  String $pg_password,
  String $pg_cluster,
  String $pg_version,
  String $hostname = $::hostname,
  String $fqdn = $::fqdn,
) {
  include postgres::backup_server::globals

  # foobar.debian.org:5432:*:debian-backup:swordfish
  @@concat::fragment { "postgresql::server::backup-source-pgpassline::${hostname}::${pg_port}::${pg_role}":
    target  => $postgres::backup_server::globals::pgpassfile,
    content => @("EOF"),
        ${fqdn}:${pg_port}:*:${pg_role}:${pg_password}
        | EOF
    tag     => $postgres::backup_server::globals::tag_source_pgpassline,
  }
  # example entry:
  #   vittoria.debian.org  5432  debian-backup    main    9.6
  @@concat::fragment { "postgresql::server::backup-source-make-base-backup-entry::${hostname}::${pg_port}::${pg_role}":
    tag     => $postgres::backup_server::globals::tag_base_backup,
    target  => $postgres::backup_server::globals::base_backup_clusters,
    content => "${::fqdn} ${pg_port} ${pg_role} ${pg_cluster} ${pg_version}",
  }

  @@file { "/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/${hostname}-${pg_cluster}.conf":
    content => @("EOF"),
        --- 
        backups:
          ${hostname}:
            ${pg_cluster}:
        | EOF
    tag     => $postgres::backup_server::globals::tag_dsa_check_backupp,
    notify  => Exec['update dsa-check-backuppg.conf']
  }
}
