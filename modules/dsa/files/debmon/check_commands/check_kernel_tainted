#!/bin/bash

set -e
set -u

[ -e /etc/os-release ] && . /etc/os-release
if [ -n "${REDHAT_SUPPORT_PRODUCT_VERSION:-}" ]; then
  REDHAT_MAJOR_VERSION=${REDHAT_SUPPORT_PRODUCT_VERSION%%.*}
fi

usage() {
  echo "Usage: $0 [-I <ignore-bit> [-I <ignore-bit>...]]"
}
print_info() {
  local bit="$1"; shift
  case "${bit}" in
    0) echo -n "[proprietary_module]";;
    1) echo -n "[forced_module]";;
    2) echo -n "[cpu_out_of_spec]";;
    3) echo -n "[forced_rmmod]";;
    4) echo -n "[machine_check]";;
    5) echo -n "[bad_page]";;
    6) echo -n "[user]";;
    7) echo -n "[DIE (oops-or-bug)]";;
    8) echo -n "[overridden_acpi_table]";;
    9) echo -n "[WARN]";;
    10) echo -n "[crap (staging-driver)]";;
    11) echo -n "[firmware_workaround]";;
    12) echo -n "[oot_module]";;
    13) echo -n "[unsigned_module]";;
    14) echo -n "[softlockup]";;
    15) echo -n "[livepatch]";;
    16) echo -n "[aux]";;
    17) echo -n "[randstruct]";;

    27) [ "${REDHAT_MAJOR_VERSION:-}" -ge "8" ] && echo -n "[support_removed (RH)]";;
    28) [ "${REDHAT_MAJOR_VERSION:-}" -le "7" ] && echo -n "[hardware_unsupported (RH)]";;
    29) [ "${REDHAT_MAJOR_VERSION:-}" -le "8" ] && echo -n "[tech_preview (RH)]";;
    30) [ "${REDHAT_MAJOR_VERSION:-}" -eq "8" ] && echo -n "[unprivileged_bpf (RH)]";;
    31) [ "${REDHAT_MAJOR_VERSION:-}" -eq "9" ] && echo -n "[unprivileged_bpf (RH)]";;
  esac
}
print_bits() {
  local value="$1"; shift
  local value_mask="$1"; shift

  local bit
  if [ "${value}" != "0" ]; then
    echo -n "bits set:"
    for bit in $(seq 0 31); do
      bitvalue=$((value>>bit&1))
      if [ "${bitvalue}" -gt 0 ]; then
        echo -n " ${bit}"
        ismasked=$((value_mask>>bit&1))
        print_info "${bit}"
        [ "${ismasked}" -gt 0 ] && echo -n "(i)"
      fi
    done
  fi
  true
}
print_extra_info() {
  echo "Consult https://docs.kernel.org/admin-guide/tainted-kernels.html"
  echo "and/or https://access.redhat.com/solutions/40594"
}

mask=0
while getopts "hI:" OPTION; do
  case "$OPTION" in
    h)
      usage
      exit 0
    ;;
    I)
      ignore_bit="$OPTARG"
      mask=$((mask | 1<<ignore_bit))
    ;;
    *)
      usage >&2
      exit 1
  esac
done
shift $((OPTIND - 1))


taint="$(cat /proc/sys/kernel/tainted)"
taint_masked=$(( taint & ~mask ))

if [ "${taint_masked}" != "0" ]; then
  echo -n "Warning: Kernel is tainted (${taint}): "
  print_bits "${taint}" "${mask}"
  echo; echo
  print_extra_info
  exit 1
elif [ "${taint}" != "0" ]; then
  echo -n "OK: Kernel is tainted (${taint}) but all bits ignored; "
  print_bits "${taint}" "${mask}"
  echo; echo
  print_extra_info
  exit 0
else
  echo "OK Kernel is not tainted (${taint})."
  exit 0
fi
