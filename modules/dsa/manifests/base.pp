# include basic setup of DSA hosts
class dsa::base {
  include roles::mta

  include munin
  include syslog_ng
  include profile::sudo
  include ssh
  include debian_org
  include ssl
  include hardware
  include nagios::client
  include roles
  include unbound
  include bacula::client
  include grub
  include multipath
  include portforwarder
  include postgres
  include certregen::client

  include dsa::base::resolv_conf
  include dsa::base::popcon
  include dsa::base::time
  include dsa::base::samhain
  include dsa::base::haveged
  include dsa::base::motd


  $kernel_cleaned = $facts['kernel'].downcase()

  $kernel_class = "${title}::kernel::${kernel_cleaned}"
  include $kernel_class
}
