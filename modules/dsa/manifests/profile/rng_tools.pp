# install rng_tools on hosts that support/want it
#
# @param ensure  install or purge the rng tools
class dsa::profile::rng_tools (
  Enum['present', 'absent'] $ensure =
    if $facts['hw_can_hwrng'] and $facts['virtual'] != 'kvm' { 'present' } else { 'absent' },
) {
  package { ['rng-tools', 'rng-tools-debian']:
    ensure => purged
  }

  package { 'rng-tools5':
    ensure => if $ensure == 'present' { 'installed' } else { 'purged' },
  }
  if $ensure == 'present' {
    service { 'rngd':
      ensure  => running,
      require => Package['rng-tools5']
    }
  }
}
