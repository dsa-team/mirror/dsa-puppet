# Various hardware related checks
class dsa::debmon::hardware (
) {
  if pick($facts.dig('processors', 'models'), []).any |$model| { $model =~ /(?i:intel)/ } {
    mon::service::shell_wrapped { 'cpu-intel-microcode': # {{{
      check_interval => '2h',
      retry_interval => '15m',
      command        => '/usr/lib/nagios/plugins/dsa-check-ucode-intel',
      sudo           => true,
    } # }}}
  }
}
# vim:set fdm=marker:
