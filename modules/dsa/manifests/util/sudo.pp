# Add a sudoers entry
#
# @param user
#   The user (or %group) that may use this entry.
# @param command
#   The command to run.
# @param run_as
#   The user to run the command as.
# @param no_password
#   Whether or not the source user need not authenticate to use this entry.
define dsa::util::sudo (
  String $user,
  String $command,
  String $run_as = 'root',
  Boolean $no_password = true,
) {
  $flags = if $no_password { 'NOPASSWD: ' } else { '' }
  sudo::conf { "dsa::util::sudo[${title}]":
    content => "${user} ALL=(${run_as}) ${flags}${command}",
  }
}
