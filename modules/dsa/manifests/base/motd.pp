# Maintain /etc/motd on debian.org hosts
#
# @param messages
#   List of messages to add to the motd.  This is for adding information
#   via hiera.  For normal puppet code, use dsa::base::motd::fragment.
class dsa::base::motd (
  Optional[Array[String]] $messages = undef,
) {
  concat{'/etc/motd':
    owner          => 'root',
    group          => 'root',
    mode           => '0444',
    ensure_newline => true,
  }

  dsa::base::motd::fragment { 'header':
    order       => '000',
    content_raw => @("EOF"),

      This device is for authorized users only.  All traffic on this device
      is monitored and will be used as evidence for prosecutions.  By using
      this machine you agree to abide by the Debian Machine Usage Policies
      <URL:https://www.debian.org/devel/dmup>.

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      | EOF
  }

  pick($messages, []).each |$idx, $m| {
    dsa::base::motd::fragment { "${title}-message-${idx}":
      content => $m,
    }
  }

  dsa::base::motd::fragment { 'legacy_block':
    content_raw => template('dsa/base/legacy_motd.erb')
  }

  dsa::base::motd::fragment { 'trailer':
    order       => '990',
    content_raw => @("EOF"),
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      | EOF
  }
}
