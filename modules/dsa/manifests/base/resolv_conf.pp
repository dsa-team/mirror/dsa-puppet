# set up /etc/resolv.conf
class dsa::base::resolv_conf (
  Optional[Array[Stdlib::IP::Address, 1]] $nameservers = undef,
  Optional[Array[String, 1]] $searchpaths = undef,
) {
  $real_nameservers = $facts['unbound'] ? {
    true    => ['127.0.0.1'],
    default => pick($nameservers, ['8.8.8.8', '2001:4860:4860::8888', '8.8.4.4']),
  }
  $unboundresolvoptions = (versioncmp($facts['os']['release']['major'], '11') >= 0) ? {
    true    => ['edns0', 'trust-ad'],
    default => ['edns0'],
  }
  $options = if $facts['unbound'] { $unboundresolvoptions } else {[]}

  $resolv_conf_template = @(EOF)
    <%- |
      Array[String] $options,
      Array[Stdlib::IP::Address::Nosubnet,1] $nameservers,
      Optional[Array[Stdlib::Fqdn, 1]] $searchpaths,
    | -%>
    <%- unless $options.empty { -%>
    options <%= $options.join(' ') %>
    <%- } -%>
    <%- $nameservers.each |$addr| { -%>
    nameserver <%= $addr %>
    <%- } -%>
    <%- if $searchpaths { -%>
    search <%= $searchpaths.join(' ') %>
    <%- } -%>
    | EOF

  file { '/etc/resolv.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => inline_epp($resolv_conf_template, {
      nameservers => $real_nameservers,
      searchpaths => $searchpaths,
      options     => $options,
    }),
  }

  file { '/etc/dhcp/dhclient-enter-hooks.d/puppet-no-resolvconf':
    ensure  => ($facts['dhclient'] and $facts['unbound']) ? {
      true  => 'present',
      false => 'absent',
    },
    owner   => 'root',
    group   => 'root',
    mode    => '0555',
    content => @("EOF"),
      make_resolv_conf() { : }
      | EOF
  }
}
