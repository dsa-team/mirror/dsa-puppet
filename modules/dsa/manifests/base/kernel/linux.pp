# common bits for all linux hosts
class dsa::base::kernel::linux {
  include ferm
  include ferm::per_host

  include entropykey
  include dsa::profile::rng_tools
  include dsa::profile::acpi

  file { '/etc/modprobe.d/hwmon.conf':
    content => 'blacklist acpi_power_meter'
  }

  include dsa::base::linux_tcp_bbr
}
