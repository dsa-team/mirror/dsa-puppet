# Enforce desired state for haveged
class dsa::base::haveged (
  Enum['present', 'absent'] $ensure = 'absent',
) {
  if $ensure == 'present' {
    ensure_packages(['haveged'])
    Package['haveged']
    -> service { 'haveged':
      ensure => running,
    }
    mon::service { 'procs/haveged':
      vars => {
        argument => '/usr/sbin/haveged',
        command  => 'haveged',
        user     => 'root',
        warning  => '1:1',
        critical => '1:1',
      }
    }
  } else {
    ensure_packages( ['haveged'], { ensure => 'purged' } )
  }

  # previously used to work around #858134
  dsa_systemd::override { 'haveged':
    ensure  => 'absent',
  }
}
