#!/usr/bin/python3

import datetime
import json
import subprocess
import sys

codes = {
    'UNKNOWN': 3,
    'CRITICAL': 2,
    'WARNING': 1,
    'OK': 0 }

def get_hosts():
    return json.loads(subprocess.check_output(['puppetserver', 'ca', 'list', '--all', '--format', 'json']))

def date_is_in_past(date):
    ts = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%SUTC')
    now = datetime.datetime.utcnow()
    return ts < now

def date_is_soon(date):
    ts = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%SUTC')
    now = datetime.datetime.utcnow()
    return ts - now < datetime.timedelta(days=30)

hosts = sorted(get_hosts()['signed'], key=lambda host: host['not_after'])
expired = [host['name'] for host in hosts if date_is_in_past(host['not_after'])]
expiring = [(host['name'], host['not_after']) for host in hosts
            if date_is_soon(host['not_after'])]

output = []
if expired:
    output.append('CRITICAL: expired cert for %s' % ', '.join(expired))
if expiring:
    output.append('WARNING: expiring cert for %s' % ', '.join('%s (%s)' % (name, date) for (name, date) in expiring))
if not output:
    output.append('OK')

print('; '.join(output))

if expired:
    sys.exit(codes['CRITICAL'])
elif expiring:
    sys.exit(codes['WARNING'])
else:
    sys.exit(codes['OK'])
