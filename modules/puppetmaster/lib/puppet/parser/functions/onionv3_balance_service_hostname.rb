# This function returns the .onion name for a given service name on the local host's onionbalance instance
module Puppet::Parser::Functions
  newfunction(:onionv3_balance_service_hostname, :type => :rvalue) do |args|
    servicename = args.shift()

    onionv3_balance_service_hostname_fact = lookupvar('onionv3_balance_service_hostname')
    return nil if onionv3_balance_service_hostname_fact.nil?

    require 'json'
    parsed = JSON.parse(onionv3_balance_service_hostname_fact)
    return parsed[servicename]
  end
end
# vim:set ts=2:
# vim:set et:
# vim:set shiftwidth=2:
