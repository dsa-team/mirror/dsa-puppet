module Puppet::Parser::Functions
  newfunction(:ldapinfo, :type => :rvalue) do |attributes|

    host = attributes.shift

    unless attributes.include?("*") or attributes.include?('hostname')
      attributes << 'hostname'
    end

    require 'net/ldap'
    ldap = Net::LDAP.new :host => 'db.debian.org', :port => 636, :encryption => :simple_tls
    ldap.bind

    results = {}
    filter = '(hostname=' + host + ')'
    begin
      ldap.search(:base => 'ou=hosts,dc=debian,dc=org', :filter => filter, :attributes => attributes).each do |x|
        # net/ldap uses symbols instead of strings as hash keys
        results[x[:hostname][0]] = x
      end
    rescue Net::LDAP::Error => e
      raise Puppet::ParseError, "LDAP error: #{e.message}\n#{e.backtrace}"
    rescue RuntimeError
      raise Puppet::ParseError, "No data returned from search"
    end
    if host == '*'
      return(results)
    else
      return(results[host])
    end
  end
end
