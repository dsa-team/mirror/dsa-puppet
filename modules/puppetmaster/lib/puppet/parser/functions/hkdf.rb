# a RFC5869 implementation:
# HMAC-based Extract-and-Expand Key Derivation Function (HKDF)
#
# function John Downey, downloaded from https://rubygems.org/gems/hkdf
# and distributed under the MIT license.


Puppet::Parser::Functions.newfunction(:hkdf, :arity => -2, :type => :rvalue) do |args|
  require 'openssl'

  secretfile = args[0]
  data = args[1]

  secret = ""
  begin
    secret = File.new(secretfile, "r").read
  rescue => e
    raise Puppet::ParseError, "Error loading secret from #{secretfile}: #{e.message}\n#{e.backtrace}"
  end

  digest = OpenSSL::Digest.new('SHA256')
  info = data
  salt = 0.chr * digest.digest_length
  prk = OpenSSL::HMAC.digest(digest, salt, secret)
  OpenSSL::HMAC.digest(digest, prk, info + 1.chr).slice(0, 32).unpack('H*').first
end

# vim:set ts=2:
# vim:set et:
# vim:set shiftwidth=2:
