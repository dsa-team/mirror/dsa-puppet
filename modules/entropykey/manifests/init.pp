class entropykey {
  if getfromhash($deprecated::nodeinfo, 'entropy_key') {
    include entropykey::provider
  } else {
    mon::service::shell_wrapped { 'stunnel4-sanity': # {{{
      command => '/usr/lib/nagios/plugins/dsa-check-stunnel-sanity',
      sudo    => true,
    } # }}}
  }

  $entropy_provider  = entropy_provider($::fqdn, $deprecated::nodeinfo)
  case $entropy_provider {
    false:   {}
    local:   { include entropykey::local_consumer }
    default: {
      class { 'entropykey::remote_consumer':
        entropy_provider => $entropy_provider,
      }
    }
  }

  mon::service { 'procs/stunnel/ekey': # {{{
    vars => {
      argument => '/usr/bin/stunnel4 /etc/stunnel/puppet-ekeyd.conf',
      command  => 'stunnel4',
      user     => 'stunnel4',
      warning  => '1:6',
      critical => '1:',
    }
  } # }}}
}
