define dnsextras::dkim_record (
	$keyfile,
	$selector,
	$hostname,
	$domain,
) {
	$snippet = gen_dkim_entry($keyfile, $selector, $hostname, $domain)
	dnsextras::entry{ "$name":
		content => $snippet,
	}
}

