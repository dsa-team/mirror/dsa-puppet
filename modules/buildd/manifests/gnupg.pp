# GPG key generation
class buildd::gnupg {
  file { '/home/buildd/.gnupg':
    ensure  => directory,
    mode    => '700',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/.gnupg/gpg.conf':
    content  => "personal-digest-preferences SHA512\nprimary-keyring pubring.gpg\n",
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/.gnupg/pubring.gpg':
    ensure  => file,
    group   => buildd,
    owner   => buildd,
  }
}
