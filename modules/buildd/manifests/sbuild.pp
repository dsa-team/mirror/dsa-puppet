# sbuild configuration, including chroots
#
# @param unshare  whether to use the unshare backend instead of schroot
class buildd::sbuild(
  Boolean $unshare = false,
) {
  include schroot

    # For proper unshare support we need a minimum baseline version of a
    # backported sbuild on bookworm.
    if $unshare and versioncmp($::lsbmajdistrelease, '12') == 0 {
      $sbuild_ensure = '>=0.85.11~bpo12+1'
    } else {
      $sbuild_ensure = installed
    }

    package { 'sbuild':
      ensure    => $sbuild_ensure,
      tag       => extra_repo,
      subscribe => Exec['apt-get update'],
    }
    package { 'libsbuild-perl':
      ensure => installed,
      tag    => extra_repo,
      before => Package['sbuild']
    }
    file { '/etc/sbuild/sbuild.conf':
      content => template('buildd/sbuild.conf.erb'),
      require => Package['sbuild'],
    }
    if $::has_srv_buildd {
      concat::fragment { 'puppet-crontab--buildd-update-schroots':
        target  => '/etc/cron.d/puppet-crontab',
        content => @(EOF)
          13 22 * * 0,3 root PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd
          | EOF
      }
    }
    exec { 'add-buildd-user-to-sbuild':
      command => 'adduser buildd sbuild',
      onlyif  => "getent group sbuild > /dev/null && ! getent group sbuild | grep '\\<buildd\\>' > /dev/null"
    }

    # Build daemons running sbuild with --chroot-mode=unshare
    if $unshare {
      file { '/etc/apt/preferences.d/sbuild':
        ensure  => versioncmp($::lsbmajdistrelease, '12') ? {
          0       => file,
          default => absent,
        },
        content => "Package: src:sbuild\nPin: release n=bookworm-backports\nPin-Priority: 990\n",
        notify  => Exec['apt-get update'],
      }
      debian_org::fact { 'sysctl_request':
        value => { 'kernel.unprivileged_userns_clone' => 1 },
      }
      file { '/etc/subuid':
        mode    => '0444',
        content => 'buildd:100000:65536',
      }
      file { '/etc/subgid':
        mode    => '0444',
        content => 'buildd:100000:65536',
      }
      package { 'uidmap':
        ensure => installed,
      }

      file { '/usr/local/bin/sbuild':
        source  => 'puppet:///modules/buildd/sbuild-wrapper',
        require => Package['sbuild'],
        mode    => '0555',
      }
    }
}
