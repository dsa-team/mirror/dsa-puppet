# undo the effects of ntpsec
class ntpsec::purge {
  package { 'ntpsec':
    ensure => purged
  }

  file { '/etc/default/ntpsec':
    ensure => absent,
  }
  file { '/etc/init.d/ntpsec':
    ensure => absent,
  }
  file { '/etc/ntpsec':
    ensure => absent,
    recurse => true,
    purge   => true,
    force   => true,
  }
  file { '/var/lib/ntpsec':
    ensure  => absent,
    recurse => true,
    purge   => true,
    force   => true,
  }
  file { '/var/log/ntpsec':
    ensure  => absent,
    recurse => true,
    purge   => true,
    force   => true,
  }

  file { '/etc/munin/plugins/ntp_kernel_err':
    ensure => absent,
  }
  file { '/etc/munin/plugins/ntp_offset':
    ensure => absent,
  }
  file { '/etc/munin/plugins/ntp_states':
    ensure => absent,
  }
  file { '/etc/munin/plugins/ntp_kernel_pll_off':
    ensure => absent,
  }
  file { '/etc/munin/plugins/ntp_kernel_pll_freq':
    ensure => absent,
  }
  file { '/usr/local/sbin/ntpsec-restart-if-required':
    ensure => absent,
  }
}
