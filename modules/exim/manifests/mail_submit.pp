# our mail_submit exim class
class exim::mail_submit {
  class { 'exim':
    use_smarthost => false,
    is_mail_submit => true,
  }

  include fail2ban::exim
  include clamav
  include exim::mail_tlsa

  ferm::rule::simple { 'dsa-submission':
    description => 'Allow submission access from the world',
    port        => '587',
  }
}
