class autofs::leaseweb(
	Boolean $morgue = false,
	Boolean $qa = false,
) {
	package { 'nfs-common': ensure => installed }

	base::linux_module { 'nfs': }
	base::linux_module { 'nfsv3': }
	base::linux_module { 'nfsv4': }

	file { '/auto.dsa':
		ensure => directory,
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw01':
		where   => '/auto.dsa/snapshot-lw01',
		what    => '172.29.188.1:/storage/snapshot-farm-202309-01',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw02':
		where   => '/auto.dsa/snapshot-lw02',
		what    => '172.29.188.2:/storage/snapshot-farm-202309-02',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw03':
		where   => '/auto.dsa/snapshot-lw03',
		what    => '172.29.188.3:/storage/snapshot-farm-202309-03',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw04':
		where   => '/auto.dsa/snapshot-lw04',
		what    => '172.29.188.4:/storage/snapshot-farm-202309-04',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw09':
		where   => '/auto.dsa/snapshot-lw09',
		what    => '172.29.188.9:/storage/snapshot-farm-202309-09',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	dsa_systemd::automount { 'auto.dsa-snapshot\x2dlw10':
		where   => '/auto.dsa/snapshot-lw10',
		what    => '172.29.188.10:/storage/snapshot-farm-202309-10',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	$ensure_morgue = $morgue ? {
		true    => present,
		default => absent,
	}

	dsa_systemd::automount { 'auto.dsa-morgue.debian.org':
		ensure  => $ensure_morgue,
		where   => '/auto.dsa/morgue.debian.org',
		what    => '172.29.188.1:/srv/morgue.debian.org',
		fstype  => 'nfs',
		options => 'ro,tcp',
	}

	$ensure_qa = $qa ? {
		true    => present,
		default => absent,
	}

	dsa_systemd::automount { 'auto.dsa-qa.debian.org':
		ensure  => $ensure_qa,
		where   => '/auto.dsa/qa.debian.org',
		what    => '172.29.188.2:/srv/qa.debian.org',
		fstype  => 'nfs',
		options => 'tcp',
	}

	# Cleanup
	package { 'autofs': ensure => purged }
	file { '/etc/auto.master.d/dsa.autofs':
		ensure => absent,
	}
	file { '/etc/auto.dsa':
		ensure => absent,
	}
        file { '/etc/auto.master.d':
                ensure => absent,
        }
}
