# = Class: roles::pubsub::client
#
# Client config files for pubsub client
#
# == Sample Usage:
#
#   include roles::pubsub::client
#
class roles::pubsub::client {

  include roles::pubsub::parameters

  $rabbit_password = $roles::pubsub::parameters::rabbit_password

  if (versioncmp($::lsbmajdistrelease, '11') >= 0) {
    $dsa_mq = [ 'python3-dsa-mq' ]
  } else {
    $dsa_mq = [ 'python-dsa-mq', 'python3-dsa-mq' ]
  }
  ensure_packages($dsa_mq, {
      ensure => installed,
      tag    => extra_repo,
  })

  roles::pubsub::config { 'homedirs':
    key      => 'dsa-homedirs',
    exchange => dsa,
    topic    => 'dsa.git.homedirs',
    vhost    => dsa,
    username => $::fqdn,
    password => $rabbit_password
  }

  roles::pubsub::config { 'replicate':
    key      => 'dsa-udreplicate',
    exchange => dsa,
    queue    => "ud-${::fqdn}",
    topic    => 'dsa.ud.replicate',
    vhost    => dsa,
    username => $::fqdn,
    password => $rabbit_password
  }

  $query = 'nodes[certname] { resources { type = "Class" and title = "Roles::Pubsub" } }'
  $mqhosts = sort(puppetdb_query($query).map |$value| { $value["certname"] })
  $mqhosts.each |$h| {
    @@mon::service::shell_wrapped { "mq-connection-${h}-${trusted['certname']}": # {{{
      tag            => "mqcheck-check-to-${h}",
      host_name      => $trusted['certname'],
      display_name   => "mq-connection-on-${h}",
      command        => "/usr/lib/nagios/plugins/dsa-check-mq-connection ${trusted['hostname']} ud dsa",
      check_interval => '60m',
      retry_interval => '15m',
    } # }}}
  }
}
