# nm.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::nm (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include roles::sso_rp

  ssl::service { 'nm.debian.org':
    notify => Exec['service apache2 reload'],
  }

  exim::vdomain { 'nm.debian.org':
    owner => 'nm',
    group => 'nm',
  }

  @@postgres::cluster::hba_entry { "nm-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['nm'],
    user     => ['nm', 'nmweb'],
    address  => $base::public_addresses,
  }

  dsa_systemd::linger { 'nm': }

  include roles::postgresql::ftp_master_dak_replica::db_guest_access::ubc
}
