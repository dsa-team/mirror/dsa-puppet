# = Class: roles::sshca
#
# Setup for SSH certificate authority
#
# == Sample Usage:
#
#   include roles::sshca
#
class roles::sshca {
  file { '/etc/ssh/userkeys/sshca':
    ensure => symlink,
    target => '/var/lib/misc/thishost/ssh-sshca',
  }

  # This directory is not backed up (i.e. pointed to /srv) for now - to ensure
  # that the signing key does not leave the machine.
  file { '/home/sshca':
    ensure => directory,
    owner  => 'sshca',
    group  => 'sshca',
    mode   => '0750',
  }

  @@concat::fragment { "sshca_keys-${::fqdn}":
    content => join($facts['sshca_keys'], "\n"),
    target  => '/etc/ssh/sshca_keys',
    tag     => 'sshca_keys',
  }
}
