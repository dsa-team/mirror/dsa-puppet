# The mail_submit role, for Debian Members to relay mail by authenticating
# and benefit from Debian's infrastructure to do so (including DKIM signature)
#
# @param type exim or postfix.  exim is our default MTA
# @param dkim_domains List of domains for which the server should provide DKIM signatures
class roles::mail_submit(
  Enum['exim', 'postfix'] $type = 'exim',
  Array[String] $dkim_domains = [],
) {
  if $type == 'exim' {
    include exim::mail_submit
  } elsif $type == 'postfix' {
    fail("mail_submit of type ${type} isn't supported yet.")
  } else {
    fail("Unexpected mail_submit type ${type}.")
  }

  $dkim_domains.each |String $domain| {
    exim::dkimdomain { $domain: }
  }
}
