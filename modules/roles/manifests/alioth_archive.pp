class roles::alioth_archive {
	include apache2
	include apache2::expires
	include apache2::rewrite
	include apache2::ssl

	apache2::site { '020-alioth-archive.debian.org':
		site   => 'alioth-archive.debian.org',
		content => template('roles/apache-alioth-archive.debian.org.erb')
	}
	ssl::service { 'alioth-archive.debian.org':
		notify => Exec['service apache2 reload'],
	}

	file { '/srv/alioth-archive.debian.org':
		ensure  => directory,
		mode    => '2755',
		owner   => "alioth-archive",
		group   => "alioth-archive",
	}

	file { '/srv/alioth-archive.debian.org/home':
		ensure  => directory,
		mode    => '2755',
		owner   => "alioth-archive",
		group   => "alioth-archive",
	}

	file { '/srv/alioth-archive.debian.org/htdocs':
		ensure  => directory,
		mode    => '2755',
		owner   => "alioth-archive",
		group   => "alioth-archive",
	}

	file { '/home/alioth-archive':
		ensure => link,
		target => '/srv/alioth-archive.debian.org/home',
	}
}
