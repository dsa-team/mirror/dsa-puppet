class roles::cdbuilder_publisher {
	include roles::cdbuilder

	file { '/mnt/nfs-cdimage':
		ensure => 'link',
		target => '/auto.dsa/cdimage',
	}
}
