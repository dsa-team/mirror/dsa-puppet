class roles::vote {
  include apache2

  ssl::service { 'vote.debian.org':
    notify => Exec['service apache2 reload'],
  }

  exim::vdomain { 'vote.debian.org':
    owner => 'secretary',
    group => 'debvote',
  }
}
