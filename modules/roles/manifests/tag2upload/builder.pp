class roles::tag2upload::builder {
  dsa_systemd::linger { 'tag2upload-builder': }

  package { ['dgit', 'dgit-infrastructure']:
    ensure  => installed,
    require => File['/etc/apt/preferences.d/dgit'],
  }

  # Recommends of above packages that are not installed by default
  package { ['autopkgtest', 'python3-pygit2', 'liburi-perl', 'podman']:
    ensure  => installed,
    require => File['/etc/apt/preferences.d/autopkgtest'],
  }

  # Additional packages
  package { ['mmdebstrap', 'dbus-user-session', 'buildah', 'slirp4netns']:
    ensure => installed,
  }

  # Unprivileged user namespace support
  package { 'uidmap':
    ensure => installed,
  }
  file { '/etc/subuid':
    mode    => '0444',
    content => 'tag2upload-builder:100000:65536',
    require => Package['uidmap'],
  }
  file { '/etc/subgid':
    mode    => '0444',
    content => 'tag2upload-builder:100000:65536',
    require => Package['uidmap'],
  }
  debian_org::fact { 'sysctl_request':
    value => { 'kernel.unprivileged_userns_clone' => 1 },
  }

  file { '/etc/apt/preferences.d/dgit':
    ensure  => versioncmp($::lsbmajdistrelease, '12') ? {
      0       => file,
      default => absent,
    },
    content => "Package: src:dgit\nPin: release n=bookworm-backports\nPin-Priority: 990\n",
    notify  => Exec['apt-get update'],
  }

  file { '/etc/apt/preferences.d/autopkgtest':
    ensure  => versioncmp($::lsbmajdistrelease, '12') ? {
      0       => file,
      default => absent,
    },
    content => "Package: src:autopkgtest\nPin: release n=bookworm-backports\nPin-Priority: 500\n",
    notify  => Exec['apt-get update'],
  }

  file { '/srv/builder.tag2upload.debian.org':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-builder',
    group  => 'tag2upload-builder',
  }

  file { '/srv/builder.tag2upload.debian.org/home':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-builder',
    group  => 'tag2upload-builder',
  }

  file { '/home/tag2upload-builder':
    ensure => link,
    target => '/srv/builder.tag2upload.debian.org/home',
  }

  file { '/etc/ssh/userkeys/tag2upload-builder':
    ensure => link,
    target => '/srv/builder.tag2upload.debian.org/home/.ssh/authorized_keys',
  }

  Ferm::Rule::Simple <<| tag == "roles::tag2upload::builder-allow-ssh" |>> {
    description => 'Allow SSH access to the tag2upload builder from adjacent services',
    port        => 22,
  }
}
