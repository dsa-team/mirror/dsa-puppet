class roles::tag2upload::manager {
  include apache2
  include apache2::proxy_http
  include apache2::rewrite
  include apache2::ssl

  dsa_systemd::linger { 'tag2upload-manager': }

  package { ['sqlite3']:
    ensure => installed,
  }

  file { '/srv/manager.tag2upload.debian.org':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-manager',
    group  => 'tag2upload-manager',
  }

  file { '/srv/manager.tag2upload.debian.org/home':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-manager',
    group  => 'tag2upload-manager',
  }

  file { '/home/tag2upload-manager':
    ensure => link,
    target => '/srv/manager.tag2upload.debian.org/home',
  }

  file { '/etc/ssh/userkeys/tag2upload-manager':
    ensure => link,
    target => '/srv/manager.tag2upload.debian.org/home/.ssh/authorized_keys',
  }

  file { '/srv/manager.tag2upload.debian.org/htdocs':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-manager',
    group  => 'tag2upload-manager',
  }

  apache2::site { '020-tag2upload.debian.org':
    site    => 'tag2upload.debian.org',
    content => template('roles/tag2upload/manager/apache-tag2upload.debian.org.erb'),
  }

  ssl::service { 'tag2upload.debian.org':
    notify => Exec['service apache2 reload'],
  }

  Ferm::Rule::Simple <<| tag == "roles::tag2upload::manager-allow-ssh" |>> {
    description => 'Allow SSH access to the tag2upload manager from adjacent services',
    port        => 22,
  }
}
