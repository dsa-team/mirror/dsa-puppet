class roles::tag2upload::oracle {
  dsa_systemd::linger { 'tag2upload-oracle': }

  package { ['dgit', 'dgit-infrastructure']:
    ensure  => installed,
    require => File['/etc/apt/preferences.d/dgit'],
  }

  # Recommends of above packages that are not installed by default
  package { ['liburi-perl']:
    ensure => installed,
  }

  file { '/etc/apt/preferences.d/dgit':
    ensure  => versioncmp($::lsbmajdistrelease, '12') ? {
      0       => file,
      default => absent,
    },
    content => "Package: src:dgit\nPin: release n=bookworm-backports\nPin-Priority: 990\n",
    notify  => Exec['apt-get update'],
  }

  file { '/srv/oracle.tag2upload.debian.org':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-oracle',
    group  => 'tag2upload-oracle',
  }

  file { '/srv/oracle.tag2upload.debian.org/home':
    ensure => directory,
    mode   => '2755',
    owner  => 'tag2upload-oracle',
    group  => 'tag2upload-oracle',
  }

  file { '/home/tag2upload-oracle':
    ensure => link,
    target => '/srv/oracle.tag2upload.debian.org/home',
  }

  @@ferm::rule::simple { "tag2upload-ssh-to-manager-from-${::fqdn}":
    tag   => 'roles::tag2upload::manager-allow-ssh',
    saddr => $base::public_addresses,
  }

  @@ferm::rule::simple { "tag2upload-ssh-to-builder-from-${::fqdn}":
    tag   => 'roles::tag2upload::builder-allow-ssh',
    saddr => $base::public_addresses,
  }
}
