# the mirrormaster needs to be able to ssh to all the syncproxies and mirrors
# to update their config
class roles::mirrormaster(
) {
  @@ferm::rule::simple { "dsa-ssh-from-mirrormaster-${::fqdn}":
    tag         => 'ssh::server::to::archvsync',
    description => 'Allow ssh access from the mirrormaster',
    port        => '22',
    saddr       => $base::public_addresses,
  }
}
