# snapshot master
#
# @param db_server  name of the db server, if not local
# @param db_port    port of the db server, if not local
class roles::snapshot_master (
  Optional[String] $db_server = undef,
  Optional[Integer] $db_port = undef,
) {
  include roles::snapshot_base
  include roles::snapshot_ssh_keygen

  ssh::authorized_key_add { 'roles::snapshot_master::to::farmsync_target':
    target_user => 'snapshot',
    key         => dig($facts, 'ssh_keys_users', 'snapshot', 'id_rsa.pub', 'line'),
    command     => '~/bin/run-sync',
    collect_tag => 'roles::snapshot::to::farmsync_target',
  }

  ssh::authorized_key_collect { 'snapshot':
    target_user => 'snapshot',
    collect_tag => 'roles::snapshot::to::master',
  }

  ensure_packages(['python3-yaml'])

  if $db_server {
    if !$db_port {
      fail ('Also need a port if we have a db_server')
    }
    @@postgres::cluster::hba_entry { "snapshot-master-${::fqdn}":
      tag      => "postgres::cluster::${db_port}::hba::${db_server}",
      pg_port  => $db_port,
      database => 'snapshot',
      user     => 'snapshot',
      address  => $base::public_addresses,
    }
  }

  dsa::util::sudo { 'archvsync-snapshot-trigger':
    user    => 'archvsync',
    run_as  => 'snapshot',
    command => '/srv/snapshot.debian.org/bin/update-trigger',
  }
}
