# release.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::release (
  String  $db_address,
  Integer $db_port,
) {
  include roles::buildd_master::db_guest_access
  include roles::udd::db_guest_access
  include roles::postgresql::ftp_master_dak_replica::db_guest_access::conova

  @@postgres::cluster::hba_entry { "release-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'release',
    user     => 'release',
    address  => $base::public_addresses,
  }
}
