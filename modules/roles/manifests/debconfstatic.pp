# debconf static websites
class roles::debconfstatic {
  ensure_packages(['libtemplate-perl', 'libtemplate-plugin-xml-perl'])
}
