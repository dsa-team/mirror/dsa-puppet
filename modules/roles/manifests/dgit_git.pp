class roles::dgit_git {
  include apache2
  include roles::dgit_sync_target

  ssl::service { 'git.dgit.debian.org':
    notify => Exec['service apache2 reload'],
  }

  apache2::site { '010-git.dgit.debian.org':
    site   => 'git.dgit.debian.org',
    source => 'puppet:///modules/roles/dgit/git.dgit.debian.org',
  }

  mon::service::shell_wrapped { 'dgit_git': # {{{
    command => 'git ls-remote -q --refs https://git.dgit.debian.org/dgit',
  } # }}}
}
