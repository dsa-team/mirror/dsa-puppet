# tracker.debian.org service
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::tracker (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include roles::sso_rp

  package { 'libapache2-mod-wsgi-py3': ensure => installed, }
  apache2::module { 'wsgi': require => Package['libapache2-mod-wsgi-py3'] }
  ssl::service { 'tracker.debian.org':
    notify => Exec['service apache2 reload'],
  }
  onion::service { 'tracker.debian.org': port => 80, target_address => 'tracker.debian.org', target_port => 80, direct => true }

  # tell the mail-relays to forward this domain to us
  exim::manualroute{ 'tracker.debian.org': }
  # and then handle it locally
  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'tracker.debian.org':
    owner => 'qa',
    group => 'qa',
  }
  exim::dkimdomain { 'tracker.debian.org': }

  @@postgres::cluster::hba_entry { "tracker-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['tracker', 'tracker-test'],
    user     => 'qa',
    address  => $base::public_addresses,
  }
}
