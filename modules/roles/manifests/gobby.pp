class roles::gobby {
  include apache2
  ssl::service { 'gobby.debian.org':
    notify   => [ Exec['service apache2 reload'], Exec['restart infinoted'] ],
    tlsaport => [443, 6523],
  }
  file { '/etc/ssl/debian-local/other-keys/gobby.debian.org.key':
    ensure  => present,
    mode    => '0440',
    group   => 'gobby',
    content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.letsencrypt_dir"]) + "/gobby.debian.org.key") %>'),
    links   => follow,
    notify  => Exec['restart infinoted'],
  }

  dsa_systemd::linger { 'gobby': }

  # We need to restart, not reload, because of https://github.com/gobby/libinfinity/issues/31
  exec { 'restart infinoted':
    # systemd >= 248 supports --machine=<username>@
    command     => 'runuser -l gobby -c "systemctl --user restart infinoted"',
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    refreshonly => true,
  }

  ferm::rule::simple { 'infinoted':
    port => 6523
  }
}
