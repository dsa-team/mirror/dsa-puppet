# snapshot farm sync target
class roles::snapshot_farmsync_target {
  include roles::snapshot_secondary

  ssh::authorized_key_collect { 'snapshot':
    target_user => 'snapshot',
    collect_tag => 'roles::snapshot::to::farmsync_target',
  }
}
