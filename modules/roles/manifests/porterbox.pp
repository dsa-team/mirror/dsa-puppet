# debian.org porterbox
class roles::porterbox {
  include porterbox
  include bacula::not_a_client

  dsa::base::motd::fragment { 'porterbox-diskspace':
    content => @("EOF").regsubst(" *\n", ' ', 'G'),
      Disk space on porter boxes is often limited.  Please respect your fellow
      porters by cleaning up after yourself and deleting schroots and
      source/build trees in your ~ as soon as feasible.  DSA reserves the right
      to recover disk space as necessary.  See
      <URL:https://dsa.debian.org/doc/schroot/> for a brief tutorial on using
      schroot.  There may be chroots for architectures other than
      ${facts['debarchitecture']} available, please list available chroots to
      check.
      | EOF
  }
}
