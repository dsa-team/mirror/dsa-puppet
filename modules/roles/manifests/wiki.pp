class roles::wiki {
  include apache2
  include roles::sso_rp

  ssl::service { 'wiki.debian.org':
    notify => Exec['service apache2 reload'],
  }
}
