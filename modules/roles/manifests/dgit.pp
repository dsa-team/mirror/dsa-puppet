# the dgit role
#
# stores the sync command to be collected by sync clients (browse and public git)
class roles::dgit() {
  ssh::authorized_key_add { 'dgit-sync':
    target_user => 'dgit-unpriv',
    key         => dig($facts, 'ssh_keys_users', 'dgit', 'id_rsa.pub', 'line'),
    command     => '/srv/dgit.debian.org/dgit-live/infra/dgit-mirror-ssh-wrap /srv/dgit.debian.org/unpriv/repos/ .git --',
    collect_tag => 'roles::dgit::sync',
  }
}
