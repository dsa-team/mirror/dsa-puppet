# qa.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
# @param qa_buildlogchecks_db_address     hostname of the postgres server for this service
# @param qa_buildlogchecks_db_port        port of the postgres server for this service
class roles::qamaster (
  String  $db_address,
  Integer $db_port,
  String  $qa_buildlogchecks_db_address = lookup('roles::buildd_master::qa_buildlogchecks_db_address'),
  Integer $qa_buildlogchecks_db_port    = lookup('roles::buildd_master::qa_buildlogchecks_db_port'),
) {
  include apache2
  include roles::sso_rp

  ssl::service { 'qa.debian.org': notify  => Exec['service apache2 reload'], }

  exim::vdomain { 'qa.debian.org':
    owner => 'qa',
    group => 'qa',
  }

  include roles::udd::db_guest_access
  include roles::postgresql::ftp_master_dak_replica::db_guest_access::ubc

  @@postgres::cluster::hba_entry { "qa-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'qa',
    user     => ['qa', 'qaweb'],
    address  => $base::public_addresses,
  }
  @@postgres::cluster::hba_entry { "qa-guest-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'qa',
    user     => 'guest',
    address  => $base::public_addresses,
    method   => 'trust',
  }
  @@postgres::cluster::hba_entry { "qa-buildlogchecks-guest-${::fqdn}":
    tag      => "postgres::cluster::${qa_buildlogchecks_db_port}::hba::${qa_buildlogchecks_db_address}",
    pg_port  => $qa_buildlogchecks_db_port,
    database => 'qa-buildlogchecks',
    user     => 'guest',
    address  => $base::public_addresses,
    method   => 'trust',
  }
}
