# our puppet master role
class roles::puppetmaster {
  include puppetmaster

  ssh::authorized_key_collect { 'dsa_wiki_buildhost':
    target_user => 'dsa',
    collect_tag => 'puppetmaster',
  }

  ssh::authorized_key_collect { 'puppetmaster':
    target_user => 'puppet',
    collect_tag => 'puppetmaster',
  }

  mon::service::shell_wrapped { 'puppetmaster-cert': # {{{
    command        => '/usr/lib/nagios/plugins/dsa-check-cert-expire /var/lib/puppet/ssl/certs/ca.pem',
    sudo           => 'puppet',
    check_interval => '60m',
    retry_interval => '15m',
  } # }}}
}
