# snapshot secondary
#
# That is any node that requires ssh access to the master,
# like sync targets or web mirrors.
class roles::snapshot_secondary {
  include roles::snapshot_base
  include roles::snapshot_ssh_keygen

  ssh::authorized_key_add { "roles::snapshot_master::from::farmsync_target::${::fqdn}":
    target_user => 'snapshot',
    key         => dig($facts, 'ssh_keys_users', 'snapshot', 'id_rsa.pub', 'line'),
    command     => '~/code/mirror/ssh-wrap master',
    collect_tag => 'roles::snapshot::to::master',
  }
}
