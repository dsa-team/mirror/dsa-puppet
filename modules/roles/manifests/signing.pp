# code signing for EFI secure boot
class roles::signing {
  ensure_packages([
    'expect',
    'pesign',
    'linux-kbuild-5.10',
    'libengine-pkcs11-openssl',
    'ykcs11',
    ], ensure => installed)
}
