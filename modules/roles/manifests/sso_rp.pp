class roles::sso_rp {
  file { '/var/lib/dsa':
    ensure => directory,
    mode   => '02755'
  }
  file { '/var/lib/dsa/sso':
    ensure => directory,
    mode   => '02755'
  }
  file { '/var/lib/dsa/sso/ca.crl':
    content => template('roles/sso_rp/ca.crl.erb'),
    notify  => Exec['service apache2 reload'],
  }
  file { '/var/lib/dsa/sso/ca.crt':
    source => 'puppet:///modules/roles/sso_rp/ca.crt',
    notify  => Exec['service apache2 reload'],
  }

  mon::service::shell_wrapped { 'sso-crl': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/dsa-check-crl-expire -w 129600 -c 86400 /var/lib/dsa/sso/ca.crl',
  } # }}}
}
