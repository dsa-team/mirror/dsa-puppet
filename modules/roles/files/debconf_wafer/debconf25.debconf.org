# do not modify - this file is maintained via puppet

AddType application/font-woff2 .woff2

Use common-debian-service-https-redirect * debconf25.debconf.org

WSGIDaemonProcess debconf25 \
  processes=3 threads=2 \
  user=www-data group=debconf-web maximum-requests=750 umask=0007 display-name=wsgi-debconf25.debconf.org \
  python-path=/srv/debconf-web/debconf25.debconf.org/dc25/:/srv/debconf-web/debconf25.debconf.org/dc25/ve/lib/python3.9/site-packages/

<VirtualHost *:443>
  ServerAdmin admin@debconf.org
  ServerName debconf25.debconf.org

  ErrorLog  /var/log/apache2/debconf25.debconf.org-error.log
  CustomLog /var/log/apache2/debconf25.debconf.org-access.log combined

  Use common-debian-service-ssl debconf25.debconf.org
  Use common-ssl-HSTS

  Header always set Referrer-Policy "same-origin"
  Header always set X-Content-Type-Options nosniff
  Header always set X-XSS-Protection "1; mode=block"
#  Header always set Access-Control-Allow-Origin: "*"

  WSGIProcessGroup debconf25
  WSGIScriptAlias / /srv/debconf-web/debconf25.debconf.org/dc25/wsgi.py
  WSGIPassAuthorization On

  <Directory /srv/debconf-web/debconf25.debconf.org/dc25>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  Alias /static/ /srv/debconf-web/debconf25.debconf.org/dc25/localstatic/
  Alias /favicon.ico /srv/debconf-web/debconf25.debconf.org/dc25/localstatic/img/favicon/favicon.ico
  <Directory /srv/debconf-web/debconf25.debconf.org/dc25/localstatic/>
    Require all granted

    # A little hacky, but it means we won't accidentally catch non-hashed filenames
    <FilesMatch ".*\.[0-9a-f]{12}\.[a-z0-9]{2,5}$">
      ExpiresActive on
      ExpiresDefault "access plus 1 year"
    </FilesMatch>
  </Directory>

  Alias /media/ /srv/debconf-web/debconf25.debconf.org/dc25/media/
  <Directory /srv/debconf-web/debconf25.debconf.org/dc25/media/>
    Require all granted
  </Directory>
</VirtualHost>

# vim: set ft=apache:
