# LVM config for the ppc hosts that make up ganeti2-osuosl.debian.org
class profile::lvm::ganeti2_osuosl {
  class { 'dsalvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
