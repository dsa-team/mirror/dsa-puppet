# ipv5 check
# @param ensure check enabled/disabled
define munin::ipv6check(
  Enum['present','absent'] $ensure = 'present',
) {
  include munin

  file { "/etc/munin/plugins/${name}":
    ensure  => $ensure,
    content => @(EOF),
                #!/bin/bash
                # This file is under puppet control
                . /usr/share/munin/plugins/ip_
                | EOF
    mode    => '0555',
    require => Package['munin-node'],
    notify  => Service['munin-node'],
  }
}
